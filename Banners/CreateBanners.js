const util = require('util');
var Braango = require('braango');
var defaultClient = Braango.ApiClient.instance;

/**
 *
 * @author braango
 *        
 *         Sample code showing how to add banners for a personnel
 *        
 */

//TEST auth token. Please contact
//sales@braango.com to have one
//created for you
var auth_token = defaultClient.authentications['auth_token'];
auth_token.apiKey = 'ISNWF0P30WM0CMK';


var apiInstance = new Braango.BannersApi();

//String | id of _sub_dealer_
var subdealerid = "subdealers2003"; 

//String | id of _personnel_
var salespersonid = "2244b9fd-ecc6-4c67-b185-fd9f420d3aa9"; 

//BannersInput |
var opts = { 
		'body': new Braango.BannersInput()  
};


/*
 * { "api_key": "ISNGvAzwuy4X7vAqrtV", "id": "any value",
 * "account_type": "partner" }
 */
var requestHeader = new Braango.RequestHeader();

//Set the account type to partner for
//virtual dealer and partner hosted
//accounts
requestHeader['account_type'] = "partner";

//This id for your tracking, will be reflected back in response.
//Doesn't get used by Braango
//If not speficied, braango will return session-id in its response
requestHeader['id'] = "banner-create-s2003";

//dealer_api_key returned
//when partner_dealer was created
requestHeader['api_key'] = "ISNMdzuNiKG7jhl9d9v";


opts["body"]["header"] = requestHeader;

var bannersInputBody = new Braango.BannersInputBody();

//Create Banners
bannersInputBody["dealer_banners"] = ["s2003db1-dealer api"];

bannersInputBody["client_banners"] = ["s2003cb1-client api"]

bannersInputBody["supervisor_banners"] = ["s2003sb1-supervisor api"];

opts["body"]["body"] = bannersInputBody;

apiInstance.createBanners(subdealerid, salespersonid, opts).then(function(data) {
	console.log('API called successfully. Returned data: ' + util.inspect(data, false, null));
}, function(error) {
	console.error(error);
});