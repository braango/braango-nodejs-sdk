const util = require('util');
var Braango = require('braango');
var defaultClient = Braango.ApiClient.instance;

/**
 *
 * @author braango
 *        
 *         Sample code showing how to get all braango
 *         numbers for subDealer
 *        
 *        
 *        
 */

//TEST auth token. Please contact
//sales@braango.com to have one
//created for you
var auth_token = defaultClient.authentications['auth_token'];
auth_token.apiKey = 'ISNWF0P30WM0CMK';

var apiInstance = new Braango.BraangonumbersApi();

//string | id of _sub_dealer_
var subdealerid = "subdealers2003"; 

//String | API Key to access this dealer's resources. 
//Value was returned when create_account api was called 
//and dealer was created first time
var apiKey = "ISNMdzuNiKG7jhl9d9v"; 

//String | Dealer or partner is accessing this API
var accountType = "partner"; 

apiInstance.getBraangoNumbers(subdealerid, apiKey, accountType).then(function(data) {
	console.log('API called successfully. Returned data: ' + util.inspect(data, false, null));
}, function(error) {
	console.error(error);
});
