const util = require('util');
var Braango = require('braango');
var defaultClient = Braango.ApiClient.instance;

/**
 *
 * @author braango
 *        
 *         Sample code showing how to get one braango number
 *         for sub dealer
 *        
 *        
 *        
 */

//TEST auth token. Please contact
//sales@braango.com to have one
//created for you
var auth_token = defaultClient.authentications['auth_token'];
auth_token.apiKey = 'ISNWF0P30WM0CMK';

var apiInstance = new Braango.BraangonumbersApi();

//string | id of _sub_dealer_
var subdealerid = "subdealers2002"; 

//string | id of braangonumberOrUuid
var braangonumberOrUuid = "329525fa-bf26-4117-9547-228e70231ced";  

//String | API Key to access this dealer's resources. 
//Value was returned when create_account api was called 
//and dealer was created first time
var apiKey = "ISNMdzuNiKG7jhl9d9v"; 

//String | Dealer or partner is accessing this API
var accountType = "partner"; 

apiInstance.getBraangoNumber(subdealerid, braangonumberOrUuid, apiKey, accountType).then(function(data) {
	console.log('API called successfully. Returned data: ' + util.inspect(data, false, null));
}, function(error) {
	console.error(error);
});