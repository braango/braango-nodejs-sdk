const util = require('util');
var Braango = require('braango');
var defaultClient = Braango.ApiClient.instance;

/**
 * @author braango
 *
 *    Sample code showing how to create
 *    one braango number for subdealer
 *    for given group
 *
 *
 */

//TEST auth token. Please contact
//sales@braango.com to have one
//created for you
var auth_token = defaultClient.authentications['auth_token'];
auth_token.apiKey = 'ISNWF0P30WM0CMK';

var apiInstance = new Braango.BraangonumbersApi();

//string | id of _sub_dealer_
var subdealerid = "subdealers2003"; 

//string | Group for which the braango number is being created
var group = "DEFAULT"; 

//BraangoNumberCreateInput | 
var opts = { 
		'body': new Braango.BraangoNumberCreateInput() 
};

/*
 * { "api_key": "ISNGvAzwuy4X7vAqrtV", "id": "any value",
 * "account_type": "partner" }
 */
var requestHeader = new Braango.RequestHeader();


//Set the account type to partner for
//virtual dealer and partner hosted
//accounts
requestHeader['account_type'] = "partner";

//This id for your tracking, will be reflected back in response.
//Doesn't get used by Braango
//If not speficied, braango will return session-id in its response
requestHeader['id'] = "create-braango-number";

//dealer_api_key returned
//when partner_dealer was created
requestHeader['api_key'] = "ISNMdzuNiKG7jhl9d9v";


opts["body"]["header"] = requestHeader;


//All all the personnel in represented
//by this group for this sub-dealer
var braangoNumberCreateInputBody= new Braango.BraangoNumberCreateInputBody();

braangoNumberCreateInputBody["all_personnel"] = true;

braangoNumberCreateInputBody["fake_braango_number"]  = true;

opts["body"]["body"] = braangoNumberCreateInputBody;


apiInstance.createBraangoNumber(subdealerid, group, opts).then(function(data) {
	console.log('API called successfully. Returned data: ' + util.inspect(data, false, null));
}, function(error) {
	console.error(error);
});