const util = require('util');
var Braango = require('braango');
var defaultClient = Braango.ApiClient.instance;

/**
 *
 * @author braango
 *        
 *         Sample code to update braango
 *         numbers for subDealer
 *        
 *        
 *        
 *        
 */

//TEST auth token. Please contact
//sales@braango.com to have one
//created for you
var auth_token = defaultClient.authentications['auth_token'];
auth_token.apiKey = 'ISNWF0P30WM0CMK';

var apiInstance = new Braango.BraangonumbersApi();

//string | id of _sub_dealer_
var subdealerid = "subdealers2003"; 

//string |
var braangonumberOrUuid = "deeca6af-5344-4961-8dcb-642421e88f03"; 

//BraangoNumberInput | 
var opts = { 
		'body': new Braango.BraangoNumberInput() 
};

/*
 * { "api_key": "ISNGvAzwuy4X7vAqrtV", "id": "any value",
 * "account_type": "partner" }
 */
var requestHeader = new Braango.RequestHeader();

//Set the account type to partner for
//virtual dealer and partner hosted
//accounts
requestHeader['account_type'] = "partner";

//This id for your tracking, will be reflected back in response.
//Doesn't get used by Braango
//If not speficied, braango will return session-id in its response
requestHeader['id'] = "Update-braango-number";

//dealer_api_key returned
//when partner_dealer was created
requestHeader['api_key'] = "ISNMdzuNiKG7jhl9d9v";


opts["body"]["header"] = requestHeader;

var braangoNumberInputBody= new Braango.BraangoNumberInputBody();

braangoNumberInputBody["all_personnel"] = false;

braangoNumberInputBody["fake_braango_number"]  = true;

braangoNumberInputBody["add"] = false;

braangoNumberInputBody["sales_person_id_multi"] = ["2244b9fd-ecc6-4c67-b185-fd9f420d3aa9"];

opts["body"]["body"] = braangoNumberInputBody;

apiInstance.updateBraangoNumber(subdealerid, braangonumberOrUuid, opts).then(function(data) {
	console.log('API called successfully. Returned data: ' + util.inspect(data, false, null));
}, function(error) {
	console.error(error);
});