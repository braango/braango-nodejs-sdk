const util = require('util');
var Braango = require('braango');
var defaultClient = Braango.ApiClient.instance;

/**
 *
 * @author braango
 *        
 *         Sample code showing how to create add VOICE number for personnel
 *        
 */

//TEST auth token. Please contact
//sales@braango.com to have one
//created for you
var auth_token = defaultClient.authentications['auth_token'];
auth_token.apiKey = 'ISNWF0P30WM0CMK';

var apiInstance = new Braango.VoiceApi();

//String | id of _sub_dealer_
var subdealerid = "subdealers2002"; 

//String | id of _personnel_
var salespersonid = "945cddce-6ef6-46e4-ac70-09375cf5165a";

//VoiceInput | 
var opts = { 
		'body': new Braango.VoiceInput() 
};


/*
 * { "api_key": "ISNGvAzwuy4X7vAqrtV", "id": "any value",
 * "account_type": "partner" }
 */
var requestHeader = new Braango.RequestHeader();

//Set the account type to partner for
//virtual dealer and partner hosted
//accounts
requestHeader['account_type'] = "partner";

//This id for your tracking, will be reflected back in response.
//Doesn't get used by Braango
//If not speficied, braango will return session-id in its response
requestHeader['id'] = "voice-create-s2002";

//dealer_api_key returned
//when partner_dealer was created
requestHeader['api_key'] = "ISNMdzuNiKG7jhl9d9v";

opts["body"]["header"] = requestHeader;

var voiceInputBody = new Braango.VoiceInputBody();

//Create new SMS number for this personnel
voiceInputBody["phone_number"] = "4088723456";

opts["body"]["body"] = voiceInputBody;


apiInstance.createVoice(subdealerid, salespersonid, opts).then(function(data) {
	console.log('API called successfully. Returned data: ' + util.inspect(data, false, null));
}, function(error) {
	console.error(error);
});