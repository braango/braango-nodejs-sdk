const util = require('util');
var Braango = require('braango');
var defaultClient = Braango.ApiClient.instance;

/**
 *
 * @author braango
 *        
 *         Sample code showing how to delete one Voice number for
 *         given personnel
 *        
 */

//TEST auth token. Please contact
//sales@braango.com to have one
//created for you
var auth_token = defaultClient.authentications['auth_token'];
auth_token.apiKey = 'ISNWF0P30WM0CMK';

var apiInstance = new Braango.VoiceApi();

//String | id of _sub_dealer_
var subdealerid = "subdealers2002"; 

//String | id of _personnel_
var salespersonid = "945cddce-6ef6-46e4-ac70-09375cf5165a";

//string | voice number
var _number = "(408) 976-3434";

//String | API Key to access this dealer's resources. 
//Value was returned when create_account api was called 
//and dealer was created first time
var apiKey = "ISNMdzuNiKG7jhl9d9v"; 

//String | Dealer or partner is accessing this API
var accountType = "partner"; 

apiInstance.deleteOneVoice(subdealerid, salespersonid, _number, apiKey, accountType).then(function(data) {
	console.log('API called successfully. Returned data: ' +  util.inspect(data, false, null));
}, function(error) {
	console.error(error);
});