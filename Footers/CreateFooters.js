const util = require('util');
var Braango = require('braango');
var defaultClient = Braango.ApiClient.instance;

/**
 *
 * @author braango
 *        
 *         Sample code showing how to add Footers for a personnel
 *        
 */

//TEST auth token. Please contact
//sales@braango.com to have one
//created for you
var auth_token = defaultClient.authentications['auth_token'];
auth_token.apiKey = 'ISNWF0P30WM0CMK';

var apiInstance = new Braango.FootersApi();

//string | id of _sub_dealer_
var subdealerid = "subdealers2002"; 

//string | id of _personnel_
var salespersonid = "d1e5cb9c-def7-4273-85c7-46a1b5e46869"; 

//FootersInput | 
var opts = { 
		'body': new Braango.FootersInput() 
};

/*
 * { "api_key": "ISNGvAzwuy4X7vAqrtV", "id": "any value",
 * "account_type": "partner" }
 */
var requestHeader = new Braango.RequestHeader();

//Set the account type to partner for
//virtual dealer and partner hosted
//accounts
requestHeader['account_type'] = "partner";

//This id for your tracking, will be reflected back in response.
//Doesn't get used by Braango
//If not speficied, braango will return session-id in its response
requestHeader['id'] = "Footer-create-s2002";

//dealer_api_key returned
//when partner_dealer was created
requestHeader['api_key'] = "ISNMdzuNiKG7jhl9d9v";

opts["body"]["header"] = requestHeader;

var footersInputBody = new Braango.FootersInputBody();

//Create Footers
footersInputBody["dealer_footers"] = ["s2003df1-dealer api"];

footersInputBody["client_footers"] = ["s2003cf1-client api"]

footersInputBody["supervisor_footers"] = ["s2003sf1-supervisor api"];

opts["body"]["body"] =  footersInputBody;

apiInstance.createFooters(subdealerid, salespersonid, opts).then(function(data) {
	console.log('API called successfully. Returned data: ' + util.inspect(data, false, null));
}, function(error) {
	console.error(error);
});