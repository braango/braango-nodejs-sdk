var Braango = require('braango');
var defaultClient = Braango.ApiClient.instance;
const util = require('util');
/**
 *
 * @author braango
 *        
 *         Sample code showing how to delete footers for a personnel
 *        
 */

//TEST auth token. Please contact
//sales@braango.com to have one
//created for you
var auth_token = defaultClient.authentications['auth_token'];
auth_token.apiKey = 'ISNWF0P30WM0CMK';

var apiInstance = new Braango.FootersApi();

//string | id of _sub_dealer_
var subdealerid = "subdealers2002"; 

//string | id of _personnel_
var salespersonid = "d1e5cb9c-def7-4273-85c7-46a1b5e46869"; 

//string | Footer type - `client` , `dealer` , `supervisor`
var footertype = "dealer"; 


//String | API Key to access this dealer's resources.
//Value was returned when create_account api 
//was called and dealer was created first time
var apiKey = "ISNMdzuNiKG7jhl9d9v"; 

//String | Dealer or partner is accessing this API
var accountType = "partner"; 

apiInstance.deleteFooters(subdealerid, salespersonid, footertype, apiKey, accountType).then(function(data) {
	console.log('API called successfully. Returned data: ' + util.inspect(data, false, null));
}, function(error) {
	console.error(error);
});