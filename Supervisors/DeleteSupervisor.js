const util = require('util');
var Braango = require('braango');
var defaultClient = Braango.ApiClient.instance;

/**
 *
 * @author braango
 *        
 *         Sample code to delete supervisors per group
 *        
 */

//TEST auth token. Please contact
//sales@braango.com to have one
//created for you
var auth_token = defaultClient.authentications['auth_token'];
auth_token.apiKey = 'ISNWF0P30WM0CMK';

var apiInstance = new Braango.SupervisorsApi();

//string | id of _sub_account_
var subdealerid = "subdealers2002";

//string | id of salesperson
var salespersonid = "aed72631-c968-4362-a9a4-ebe5bef8310b"; 

//string | group
var group = "Test Group";

//String | API Key to access this dealer's resources. 
//Value was returned when create_account api was 
//called and dealer was created first time
var apiKey = "ISNMdzuNiKG7jhl9d9v";

//String | Dealer or partner is accessing this API
var accountType = "partner"; 


apiInstance.deleteSupervisor(subdealerid, salespersonid, group, apiKey, accountType).then(function(data) {
	console.log('API called successfully. Returned data: ' + util.inspect(data, false, null));
}, function(error) {
	console.error(error);
});