const util = require('util');
var Braango = require('braango');
var defaultClient = Braango.ApiClient.instance;

/**
 *
 * @author braango
 *        
 *         Sample code showing how to add supervisor
 *         for personnel for given group
 *        
 *         Note supervisor is another
 *         personnel within the system
 *        
 *         Both personnel i.e. supervisor
 *         and personnel to be supervised
 *         need to exist in the system
 *        
 */

//TEST auth token. Please contact
//sales@braango.com to have one
//created for you
var auth_token = defaultClient.authentications['auth_token'];
auth_token.apiKey = 'ISNWF0P30WM0CMK';

var apiInstance = new Braango.SupervisorsApi();

//string | id of _sub_account_
var subdealerid = "subdealers2002"; 

//string | id of salesperson
var salespersonid = "aed72631-c968-4362-a9a4-ebe5bef8310b"; 

//string | group
var group = "Test Group";

//SupervisorInput | 
var opts = { 
		'body': new Braango.SupervisorInput() 
};

/*
 * { "api_key": "ISNGvAzwuy4X7vAqrtV", "id": "any value",
 * "account_type": "partner" }
 */
var requestHeader = new Braango.RequestHeader();

//Set the account type to partner for
//virtual dealer and partner hosted
//accounts
requestHeader['account_type'] = "partner";

//This id for your tracking, will be reflected back in response.
//Doesn't get used by Braango
//If not speficied, braango will return session-id in its response
requestHeader['id'] = "group-create-s2002r4";

//dealer_api_key returned
//when partner_dealer was created
requestHeader['api_key'] = "ISNMdzuNiKG7jhl9d9v";

opts["body"]["header"] = requestHeader;

var supervisorInputBody = new Braango.SupervisorInputBody();

supervisorInputBody["supervisor"] = "945cddce-6ef6-46e4-ac70-09375cf5165a"

opts["body"]["body"] = supervisorInputBody;

apiInstance.createSupervisor(subdealerid, salespersonid, group, opts).then(function(data) {
	console.log('API called successfully. Returned data: ' + util.inspect(data, false, null));
}, function(error) {
	console.error(error);
});