const util = require('util');
var Braango = require('braango');
var defaultClient = Braango.ApiClient.instance;

//TEST auth token. Please contact
//sales@braango.com to have one
//created for you
var auth_token = defaultClient.authentications['auth_token'];
auth_token.apiKey = 'ISNWF0P30WM0CMK';

var apiInstance = new Braango.PersonnelsApi();

//String | Sub dealer for which this sales person belongs to.
var subdealerid = "subdealers2002"; 

//String | Sales person ID that was returned when this personnel was created
var salespersonid = "945cddce-6ef6-46e4-ac70-09375cf5165a"; 

//PersonnelUpdateRequestInput | 
var opts = { 

		'body': new Braango.PersonnelUpdateRequestInput() 
};

var typeAdfCRMEmail = false;

/*
 * { "api_key": "ISNGvAzwuy4X7vAqrtV", "id": "any value",
 * "account_type": "partner" }
 */
var requestHeader = new Braango.RequestHeader();


//Set the account type to partner for
//virtual dealer and partner hosted
//accounts
requestHeader['account_type'] = "partner";

//This id for your tracking, will be reflected back in response.
//Doesn't get used by Braango
//If not speficied, braango will return session-id in its response
requestHeader['id'] = "update-personnel-s2002r2";

//dealer_api_key returned
//when partner_dealer was created
requestHeader['api_key'] = "ISNMdzuNiKG7jhl9d9v";

opts["body"]["header"] = requestHeader;

var personnelUpdateBody = new Braango.PersonnelUpdate();


//Required field . Used for
//SMS login in to the UI
//For Braango Enterprise, this is don't care
//unless partner implements UI with SMS login
personnelUpdateBody['sms_login'] = false;

/*
 * Password will be encrypted with SHA-25 and base64 encoded and stored
 * internally within the braango system. pattern:
 * ^(?=^.{6,10}$)(?=.*\d)(
 * ?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_+}{&quot
 * ;:;'?/>.<,])(?!.*\s).*$
 *
 * Used for single sign on. needs to 6-10 characters with one capital,
 * one numberal and one special character
 */
personnelUpdateBody['password'] = "test1T$";

personnelUpdateBody['email'] = "s2002@subdealer2002.com";

//If email specified is that for
//CRM email (ADF XML compliant)
personnelUpdateBody['type_adf_crm'] = typeAdfCRMEmail;

/*
 * Number where dealer's root account can be reached via SMS
 * for leads
 *
 * pattern:^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$
 */
personnelUpdateBody['phone_number'] = "4089763492";

/*
 * Number where dealer's root account can be reached via SMS
 * for leads
 *
 * pattern:^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$
 */
personnelUpdateBody["sms_number"] = "4089763492";

//Subscribe to the group
personnelUpdateBody["group"] = "s2002g";

//OPTIONAL FIELDS

//List of dealer banners. Braango will
//randomly choose one when sending
//message to dealer via SMS
personnelUpdateBody["dealer_banners"] = ["s2002db1updated"];


//List of client banners. Braango
//will randomly choose one when
//sending dealer messages to client
personnelUpdateBody["client_banners"] = ["s2002cb1updated"]


//List of dealer footers. Braango will
//randomly choose one when sending
//message to dealer via SMS
personnelUpdateBody["dealer_footers"] = ["s2002df1updated"];

//List of client footers. Braango
//will randomly choose one when
//sending dealer messages to client
personnelUpdateBody["client_footers"] = ["s2002cf1updated"];

//List of supervisor banners. Braango
//will randomly choose one when
//sending messages to supervisor
personnelUpdateBody["supervisor_banners"] = ["s2002sb1updated"];


//List of supervisor footers. Braango
//will randomly choose one when
//sending messages to supervisor
personnelUpdateBody["supervisor_footers"] = ["s2002sf1updated"];


opts["body"]["body"] = personnelUpdateBody;

apiInstance.updatePersonnel(subdealerid, salespersonid, opts).then(function(data) {
	console.log('API called successfully. Returned data: ' + util.inspect(data, false, null));
}, function(error) {
	console.error(error);
});