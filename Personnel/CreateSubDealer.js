const util = require('util');
var Braango = require('braango');
var defaultClient = Braango.ApiClient.instance;

/**
 *
 * @author braango
 *        
 *         Sample code showing how to create subDealer
 *        
 */

//TEST auth token. Please contact
//sales@braango.com to have one
//created for you
var auth_token = defaultClient.authentications['auth_token'];
auth_token.apiKey = 'ISNWF0P30WM0CMK';

var apiInstance = new Braango.PersonnelsApi();

//SubDealerRequestInput | 
var opts = { 
		'body': new Braango.SubDealerRequestInput()
};
var typeAdfCRMEmail = false;

/*
 * { "api_key": "ISNGvAzwuy4X7vAqrtV", "id": "any value",
 * "account_type": "partner" }
 */
var requestHeader = new Braango.RequestHeader();

//Set the account type to partner for
//virtual dealer and partner hosted
//accounts
requestHeader['account_type'] = "partner";

//This id for your tracking, will be reflected back in response.
//Doesn't get used by Braango
//If not speficied, braango will return session-id in its response
requestHeader['id'] = "create-sub-dealer-s2003";

//dealer_api_key returned
//when partner_dealer was created
requestHeader['api_key'] = "ISNMdzuNiKG7jhl9d9v";


opts["body"]["header"] = requestHeader;

var subDealerBody = new Braango.SubDealerBody();

subDealerBody['personnel_name'] = "name of manager";

subDealerBody["dealer_name"] = "test dealer s2003"

//	Required field . Used for
//	SMS login in to the UI
//	For Braango Enterprise, this is don't care
//	unless partner implements UI with SMS login
subDealerBody['sms_login'] = false;

/*
 * This is a user name created while signing this personnel up.
 * Typically this user name can be used to log into the braango UI.
 * However for whitelabel product, it is expected that this will be used
 * for single signon with respect to dealer account on partner system.
 * i.e. it is expected that partner will pass on the same user name that
 * dealer has on its system to give seamless integration experience.
 */
subDealerBody['user_name'] = "subdealer2003";


/*
 * Password will be encrypted with SHA-25 and base64 encoded and stored
 * internally within the braango system. pattern:
 * ^(?=^.{6,10}$)(?=.*\d)(
 * ?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_+}{&quot
 * ;:;'?/>.<,])(?!.*\s).*$
 *
 * Used for single sign on. needs to 6-10 characters with one capital,
 * one numberal and one special character
 */
subDealerBody['password'] = "test1T$";


/*
 * pattern: ^(?:Starter|Small Business|Rooftop|Franchise)$
 *
 * Every sub_dealer needs to have braango package
 */
subDealerBody['package'] = "Small Business";


//Required field. Indicates the
//ID for this business
//All internal resources, leads, personnel,
//braango number are associated with this id
//Needs to be unique within Braango system

//Will return error if there is clash

//Recommended to use unique naming convention
//or UUID string
subDealerBody['sub_dealer_id'] = "subdealers2003"


subDealerBody['email'] = "s2003@subdealer2003.com";



//If email specified is that for
//CRM email (ADF XML compliant)
subDealerBody['type_adf_crm'] = typeAdfCRMEmail;

/*
 * Number where dealer's root account can be reached via SMS
 * for leads
 *
 * pattern:^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$
 */
subDealerBody['phone_number'] = "4089763499";

/*
 * Number where dealer's root account can be reached via SMS
 * for leads
 *
 * pattern:^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$
 */
subDealerBody["sms_number"] = "4089763499";

//Subscribe to the group
subDealerBody["group"] = "s2003g";

//OPTIONAL FIELDS

//List of dealer banners. Braango will
//randomly choose one when sending
//message to dealer via SMS
subDealerBody["dealer_banners"] = ["s2003db1"];


//List of client banners. Braango
//will randomly choose one when
//sending dealer messages to client
subDealerBody["client_banners"] = ["s2003cb1"]


//List of dealer footers. Braango will
//randomly choose one when sending
//message to dealer via SMS
subDealerBody["dealer_footers"] = ["s2003df1"];

//List of client footers. Braango
//will randomly choose one when
//sending dealer messages to client
subDealerBody["client_footers"] = ["s2003cf1"];

//List of supervisor banners. Braango
//will randomly choose one when
//sending messages to supervisor
subDealerBody["supervisor_banners"] = ["s2003sb1"];


//List of supervisor footers. Braango
//will randomly choose one when
//sending messages to supervisor
subDealerBody["supervisor_footers"] = ["s2003sf1"];


opts["body"]["body"] = subDealerBody;




apiInstance.createSubDealer(opts).then(function(data) {
	console.log('API called successfully. Returned data: ' + util.inspect(data, false, null));
}, function(error) {
	console.error(error);
});