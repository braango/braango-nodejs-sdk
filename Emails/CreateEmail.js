const util = require('util');
var Braango = require('braango');
var defaultClient = Braango.ApiClient.instance;

/**
 *
 * @author braango
 *        
 *         Sample code showing how to create add email for personnel
 *        
 */

//TEST auth token. Please contact
//sales@braango.com to have one
//created for you
var auth_token = defaultClient.authentications['auth_token'];
auth_token.apiKey = 'ISNWF0P30WM0CMK';

var apiInstance = new Braango.EmailsApi();

//String | id of _sub_dealer_
var subdealerid = "subdealers2002";

//String | if of _personnel_
var salespersonid = "aed72631-c968-4362-a9a4-ebe5bef8310b";

//EmailInput | 
var opts = { 
		'body': new Braango.EmailInput()
};

var typeAdfCRMEmail = false;

/*
 * { "api_key": "ISNGvAzwuy4X7vAqrtV", "id": "any value",
 * "account_type": "partner" }
 */
var requestHeader = new Braango.RequestHeader();

//Set the account type to partner for
//virtual dealer and partner hosted
//accounts
requestHeader['account_type'] = "partner";

//This id for your tracking, will be reflected back in response.
//Doesn't get used by Braango
//If not speficied, braango will return session-id in its response
requestHeader['id'] = "email-create-s2002r1";

//dealer_api_key returned
//when partner_dealer was created
requestHeader['api_key'] = "ISNMdzuNiKG7jhl9d9v";

opts["body"]["header"] = requestHeader;

var emailInputBody = new Braango.EmailInputBody();

emailInputBody["email"] = "test3@subdealers2002.com";

//If email specified is that for
//CRM email (ADF XML compliant)
emailInputBody['type_adf_crm'] = typeAdfCRMEmail;

opts["body"]["body"] = emailInputBody;

apiInstance.createEmail(subdealerid, salespersonid, opts).then(function(data) {
	console.log('API called successfully. Returned data: ' + util.inspect(data, false, null));
}, function(error) {
	console.error(error);
});