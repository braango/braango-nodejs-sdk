const util = require('util');
var Braango = require('braango');
var defaultClient = Braango.ApiClient.instance;

/**
 *
 * @author braango
 *        
 *         Sample code showing how to delete all email for personnel
 *        
 */

//TEST auth token. Please contact
//sales@braango.com to have one
//created for you
var auth_token = defaultClient.authentications['auth_token'];
auth_token.apiKey = 'ISNWF0P30WM0CMK';

var apiInstance = new Braango.EmailsApi();

//String | id of _sub_dealer_
var subdealerid = "subdealers2002";

//String | if of _personnel_
var salespersonid = "aed72631-c968-4362-a9a4-ebe5bef8310b"; 

//String | API Key to access this dealer's resources.
//Value was returned when create_account api was called 
//and dealer was created first time
var apiKey = "ISNMdzuNiKG7jhl9d9v"; 

//String | Dealer or partner is accessing this API
var accountType = "partner"; 

//Boolean | Specifies to get ADF CRM email records or plain email records. 
//If not specified, default is false i.e. regular email ID accounts
var opts = { 
		'typeAdfCrm': false
};

apiInstance.deleteEmail(subdealerid, salespersonid, apiKey, accountType, opts).then(function(data) {
	console.log('API called successfully. Returned data: ' + util.inspect(data, false, null));
}, function(error) {
	console.error(error);
});