var Braango = require('braango');
var defaultClient = Braango.ApiClient.instance;

/**
 *
 * @author braango
 *        
 *         Sample code showing how to use
 *         addClient api to add new
 *         client to a sub-dealer
 *        
 *         The api is very flexible with
 *         lots of options
 *        
 *         Please refer to api docs
 *         to see how to use all options
 *        
 *         Intent here is to show
 *         the relevant api and
 *         call needed
 *        
 *        
 */

//TEST auth token. Please contact
//sales@braango.com to have one
//created for you
var auth_token = defaultClient.authentications['auth_token'];
auth_token.apiKey = 'ISNWF0P30WM0CMK';

var apiInstance = new Braango.ConnectsApi();

//string | id of _sub_dealer_
var subdealerid = "subdealers2002"; 

//String | salespersonid
var salespersonid = "077db60c-7e6c-49ee-a137-aa7d53a88cba"; 

//AddClientRequestInput |
var opts = { 
		'body': new Braango.AddClientRequestInput()  
};

/*
 * { "api_key": "ISNGvAzwuy4X7vAqrtV", "id": "any value",
 * "account_type": "partner" }
 */
var requestHeader = new Braango.RequestHeader();

//Set the account type to partner for
//virtual dealer and partner hosted
//accounts
requestHeader['account_type'] = "partner";

//This id for your tracking, will be reflected back in response.
//Doesn't get used by Braango
//If not speficied, braango will return session-id in its response
requestHeader['id'] = "add-client-02";

//dealer_api_key returned
//when partner_dealer was created
requestHeader['api_key'] = "ISNMdzuNiKG7jhl9d9v";


opts["body"]["header"] = requestHeader;

var addClientBody = new Braango.AddClient();


//This is the client that is going
//to be seeded into the
//braango system for this subdealer and/or
//given personnel

//If the client already exists, it simply
//becomes upsert operation
addClientBody["client_number"] = "6692459240";

//Specifies if the added client
//is to be connected with
//the dealer. Default is
//true. If false
//then, to enable connection
//either call this api or dealerconnect
//api and specify enable = true
addClientBody["connect_enable"] = false;

//specify the braango number
//to be used. System will
//verify if the braango number
//is valid for given sub_dealer and/or personnel

//If not specified, partner's blast number will be
//used

//As a fallback Braango's general number will be used
//if everything fails
addClientBody["braango_number"] = "555-555-5555";

//Allows to send initial seeding message
//to the client if this flag is true
addClientBody["send_message"] = true;


//If this flag is true, seeding will not
//happen. This should be used after
//seeding has happened and
//api caller wants programmatic
//control of sending message to the
//client
addClientBody["send_message_only"] = false;

addClientBody["message"] = ["Line 1"];


opts["body"]["body"] = addClientBody;

apiInstance.addClient(subdealerid, salespersonid, opts).then(function() {
	console.log('API called successfully.');
}, function(error) {
	console.error(error);
});