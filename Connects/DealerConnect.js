var Braango = require('braango');
var defaultClient = Braango.ApiClient.instance;

/**
 *
 * @author braango
 *        
 *         Sample code showing how to use
 *         dealerConnect api to enable
 *         bridge between seeded client
 *         and the sub-dealer
 */

//TEST auth token. Please contact
//sales@braango.com to have one
//created for you
var auth_token = defaultClient.authentications['auth_token'];
auth_token.apiKey = 'ISNWF0P30WM0CMK';


var apiInstance = new Braango.ConnectsApi();

//string | id of _sub_dealer_
var subdealerid = "subdealers2002"; 

//string | id of _personnel_
var salespersonid = "077db60c-7e6c-49ee-a137-aa7d53a88cba"; 

//DealerConnectRequestInput | 
var opts = { 
		'body': new Braango.DealerConnectRequestInput() 
};

/*
 * { "api_key": "ISNGvAzwuy4X7vAqrtV", "id": "any value",
 * "account_type": "partner" }
 */
var requestHeader = new Braango.RequestHeader();

//Set the account type to partner for
//virtual dealer and partner hosted
//accounts
requestHeader['account_type'] = "partner";

//This id for your tracking, will be reflected back in response.
//Doesn't get used by Braango
//If not speficied, braango will return session-id in its response
requestHeader['id'] = "add-dealer-01";

//dealer_api_key returned
//when partner_dealer was created
requestHeader['api_key'] = "ISNMdzuNiKG7jhl9d9v";


opts["body"]["header"] = requestHeader;

var dealerConnectBody = new Braango.DealerConnect();

//This is the client that is going
//to be seeded into the
//braango system for this subdealer and/or
//given personnel

//If the client already exists, it simply
//becomes upsert operation

//This also informs braango
//which client to use for connecting
//to the subDealer personnel
dealerConnectBody["client_number"] = "6692459240";


//Specifies if the client
//is to be connected with
//the dealer.
dealerConnectBody["connect_enable"] = true;

//specify the braango number
//to be used. System will
//verify if the braango number
//is valid for given sub_dealer and/or personnel

//If not specified, partner's blast number will be
//used

//As a fallback Braango's general number will be used
//if everything fails
dealerConnectBody["braango_number"] = "555-555-5555";

dealerConnectBody["message"] = ["Line 1"];


opts["body"]["body"] = dealerConnectBody;

apiInstance.dealerConnect(subdealerid, salespersonid, opts).then(function() {
	console.log('API called successfully.');
}, function(error) {
	console.error(error);
});