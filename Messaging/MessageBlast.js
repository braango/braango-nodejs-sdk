const util = require('util');
var Braango = require('braango');
var defaultClient = Braango.ApiClient.instance;

/**
 *
 * @author braango
 *        
 *         Sample code showing how to blast
 *         to multiple consumers via
 *         partner's blast number (different than
 *         braango number)
 *        
 *        
 */

//TEST auth token. Please contact
//sales@braango.com to have one
//created for you
var auth_token = defaultClient.authentications['auth_token'];
auth_token.apiKey = 'ISNWF0P30WM0CMK';

var apiInstance = new Braango.MessagingApi();

//MessageBlastRequestInput | 
var opts = { 
		'body': new Braango.MessageBlastRequestInput() 
};

/*
 * { "api_key": "ISNGvAzwuy4X7vAqrtV", "id": "any value",
 * "account_type": "partner" }
 */
var requestHeader = new Braango.RequestHeader();

//Set the account type to partner for
//virtual dealer and partner hosted
//accounts
requestHeader['account_type'] = "partner";

//This id for your tracking, will be reflected back in response.
//Doesn't get used by Braango
//If not speficied, braango will return session-id in its response
requestHeader['id'] = "message-blast-01";

//dealer_api_key returned
//when partner_dealer was created
requestHeader['api_key'] = "ISNMdzuNiKG7jhl9d9v";


opts["body"]["header"] = requestHeader;

var messageBlast = new Braango.MessageBlast();

var messageBlastBody = new Braango.MessageBlastNumbers();

messageBlastBody["message"] = ["Line 1"];

messageBlastBody["number_id"] = "required-unique-id";

messageBlastBody["phone_number"] = "6692459240";

messageBlast["numbers"] = [messageBlastBody];

opts["body"]["body"] = messageBlast;

apiInstance.messageBlast(opts).then(function(data) {
	console.log('API called successfully. Returned data: ' + util.inspect(data, false, null));
}, function(error) {
	console.error(error);
});