const util = require('util');
var Braango = require('braango');
var defaultClient = Braango.ApiClient.instance;

/**
 *
 * @author braango
 *        
 *         Sample code showing how to get all webhooks
 *        
 *        
 *        
 */

//TEST auth token. Please contact
//sales@braango.com to have one
//created for you
var auth_token = defaultClient.authentications['auth_token'];
auth_token.apiKey = 'ISNWF0P30WM0CMK';

var apiInstance = new Braango.WebhooksApi();

//String | id of _sub_dealer_
var subdealerid = "subdealers2002"; 

//String | id of _personnel_
var salespersonid = "aed72631-c968-4362-a9a4-ebe5bef8310b";

//WebhookInput | 
var opts = { 
		'body': new Braango.WebhookInput() 
};

/*
 * { "api_key": "ISNGvAzwuy4X7vAqrtV", "id": "any value",
 * "account_type": "partner" }
 */
var requestHeader = new Braango.RequestHeader();

//Set the account type to partner for
//virtual dealer and partner hosted
//accounts
requestHeader['account_type'] = "partner";

//This id for your tracking, will be reflected back in response.
//Doesn't get used by Braango
//If not speficied, braango will return session-id in its response
requestHeader['id'] = "create-webhook";

//dealer_api_key returned
//when partner_dealer was created
requestHeader['api_key'] = "ISNMdzuNiKG7jhl9d9v";


opts["body"]["header"] = requestHeader;

var webhookInputBody = new Braango.WebhookInputBody();

webhookInputBody["auth_id"] = "webhook-auth-id";

webhookInputBody["auth_key"] = "partner-webhook-auth-key";

webhookInputBody["enable"] = true;

webhookInputBody["post_url"] = "https://test.partner.webhook/someresource";

opts["body"]["body"] = webhookInputBody;


apiInstance.createWebhook(subdealerid, salespersonid, opts).then(function(data) {
	console.log('API called successfully. Returned data: ' + util.inspect(data, false, null));
}, function(error) {
	console.error(error);
});